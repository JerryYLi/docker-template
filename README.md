# docker-template

- Fork this repo
- Add your conda environment as `environment.yml`
- CI/CD pipeline should start running automatically
- After pipeline completes, container can be accessed at `registry.gitlab.com/[username]/[reponame]:latest`
- Feel free to create new branches for different environments. Use `registry.gitlab.com/[username]/[reponame]:[branch]` for images from specific branches