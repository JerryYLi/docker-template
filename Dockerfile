FROM nvidia/cuda:11.7.1-cudnn8-runtime-ubuntu22.04
RUN apt-get update && \
    apt-get install -y build-essential gcc libglib2.0-0 libsm6 libxext6 libxrender-dev libgl1-mesa-glx vim git curl wget zsh unzip default-jre && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install conda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-py39_22.11.1-1-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /venv && \
    rm ~/miniconda.sh && \
    /venv/bin/conda clean -afy && \
    ln -s /venv/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /venv/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc && \
    find /venv/ -follow -type f -name '*.a' -delete && \
    find /venv/ -follow -type f -name '*.js.map' -delete && \
    /venv/bin/conda clean -afy && \
    rm -rf /root/.cache/pip/* && \
    rm -rf /tmp/*

# Add extra python packages
ENV PATH=/venv/bin:$PATH
RUN pip install --upgrade pip
RUN conda update conda
COPY environment.yml /tmp/
RUN conda env update -n base -f /tmp/environment.yml
